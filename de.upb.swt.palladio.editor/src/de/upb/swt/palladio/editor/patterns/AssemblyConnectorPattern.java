package de.upb.swt.palladio.editor.patterns;

import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;

import de.uka.ipd.sdq.pcm.core.composition.AssemblyConnector;
import de.uka.ipd.sdq.pcm.core.composition.CompositionFactory;
import de.uka.ipd.sdq.pcm.repository.OperationProvidedRole;
import de.uka.ipd.sdq.pcm.repository.OperationRequiredRole;
import de.uka.ipd.sdq.pcm.repository.ProvidedRole;
import de.uka.ipd.sdq.pcm.repository.RequiredRole;

public class AssemblyConnectorPattern extends ConnectorPattern {

	public String getCreateName() {
		return "Assembly Connector";
	}

	public AssemblyConnectorPattern() {
		super();
	}

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		// return true if both anchors belong to a EClass
		// and those EClasses are not identical
		RequiredRole source = getOperationRequiredRole(context
				.getSourceAnchor());
		ProvidedRole target = getOperationProvidedRole(context
				.getTargetAnchor());
		if (source != null && target != null && source != target) {
			return true;
		}
		return false;
	}

	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		// return true if start anchor belongs to a EClass
		if (getOperationRequiredRole(context.getSourceAnchor()) != null) {
			return true;
		}
		return false;
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		Connection newConnection = null;

		// get EClasses which should be connected
		RequiredRole source = getOperationRequiredRole(context
				.getSourceAnchor());
		ProvidedRole target = getOperationProvidedRole(context
				.getTargetAnchor());

		if (source != null && target != null) {
			// create new business object
			AssemblyConnector connector = CompositionFactory.eINSTANCE
					.createAssemblyConnector();
			connector
					.setRequiredRole_AssemblyConnector((OperationRequiredRole) source);
			connector
					.setProvidedRole_AssemblyConnector((OperationProvidedRole) target);

			// add connection for business object
			AddConnectionContext addContext = new AddConnectionContext(
					context.getSourceAnchor(), context.getTargetAnchor());
			addContext.setNewObject(connector);
			newConnection = (Connection) getFeatureProvider().addIfPossible(
					addContext);
		}

		return newConnection;
	}

	/**
	 * Returns the EClass belonging to the anchor, or null if not available.
	 */
	// private EClass getEClass(Anchor anchor) {
	// if (anchor != null) {
	// Object obj = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (obj instanceof EClass) {
	// return (EClass) obj;
	// }
	// }
	// return null;
	// }

	private OperationProvidedRole getOperationProvidedRole(Anchor anchor) {
		if (anchor != null) {
			Object obj = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (obj instanceof OperationProvidedRole) {
				return (OperationProvidedRole) obj;
			}
		}
		return null;
	}

	private OperationRequiredRole getOperationRequiredRole(Anchor anchor) {
		if (anchor != null) {
			Object obj = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (obj instanceof OperationRequiredRole) {
				return (OperationRequiredRole) obj;
			}
		}
		return null;
	}

	/**
	 * Creates a EReference between two EClasses.
	 */
	// private EReference createEReference(EClass source, EClass target) {
	// EReference eReference = EcoreFactory.eINSTANCE.createEReference();
	//		eReference.setName("new EReference"); //$NON-NLS-1$
	// eReference.setEType(target);
	// eReference.setLowerBound(0);
	// eReference.setUpperBound(1);
	// source.getEStructuralFeatures().add(eReference);
	// return eReference;
	// }


	@Override
	public boolean canAdd(IAddContext context) {
		// return true if given business object is an EReference
		// note, that the context must be an instance of IAddConnectionContext
		if (context instanceof IAddConnectionContext
				&& context.getNewObject() instanceof AssemblyConnector) {
			return true;
		}
		return false;
	}


}
