package de.upb.swt.palladio.editor.patterns;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.IRemoveContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.BoxRelativeAnchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.pattern.AbstractPattern;
import org.eclipse.graphiti.pattern.IPattern;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.services.IPeService;
import de.uka.ipd.sdq.pcm.core.composition.AssemblyContext;
import de.uka.ipd.sdq.pcm.core.composition.CompositionFactory;
import de.uka.ipd.sdq.pcm.repository.InfrastructureProvidedRole;
import de.uka.ipd.sdq.pcm.repository.InfrastructureRequiredRole;
import de.uka.ipd.sdq.pcm.repository.OperationProvidedRole;
import de.uka.ipd.sdq.pcm.repository.OperationRequiredRole;
import de.uka.ipd.sdq.pcm.repository.ProvidedRole;
import de.uka.ipd.sdq.pcm.repository.RequiredRole;
import de.uka.ipd.sdq.pcm.repository.Role;
import de.upb.swt.palladio.editor.StyleUtil;

public class AssemblyContextPattern extends AbstractPattern implements IPattern {

	private static final int MIN_HEIGHT = 80;

	private static final int MIN_WIDTH = 100;

	private static final int COMP_SYMBOL_WIDTH = 20;

	private static final int COMP_SYMBOL_HEIGHT = 20;

	private enum RoleDirection {
		LEFT, RIGHT, UP, DOWN
	};

	// private static final int[] OPERATION_REQUIRED_ROLE_COORDS_UP = new int[]
	// {
	// 0, 10, 10, 20, 20, 10};
	private static final int[] OPERATION_REQUIRED_ROLE_COORDS_UP = new int[] {
			0, 0, 1, 4, 2, 6, 3, 7, 4, 8, 5, 8, 6, 9, 7, 9, 8, 9, 9, 9, 10, 10,
			11, 9, 12, 9, 13, 9, 14, 9, 15, 8, 16, 8, 17, 7, 18, 6, 19, 4, 20,
			0 };

	private static final int[] OPERATION_REQUIRED_ROLE_COORDS_LEFT = new int[] {
			0, 0, 4, 1, 6, 2, 7, 3, 8, 4, 8, 5, 9, 6, 9, 7, 9, 8, 9, 9, 10, 10,
			9, 11, 9, 12, 9, 13, 9, 14, 8, 15, 8, 16, 7, 17, 6, 18, 4, 19, 0,
			20 };

	private static final int[] OPERATION_REQUIRED_ROLE_COORDS_DOWN = new int[] {
			0, 10, 1, 6, 2, 4, 3, 3, 4, 2, 5, 2, 6, 1, 7, 1, 8, 1, 9, 1, 10, 0,
			11, 1, 12, 1, 13, 1, 14, 1, 15, 2, 16, 2, 17, 3, 18, 4, 19, 6, 20,
			10 };

	private static final int[] OPERATION_REQUIRED_ROLE_COORDS_RIGHT = new int[] {
			10, 0, 6, 1, 4, 2, 3, 3, 2, 4, 2, 5, 1, 6, 1, 7, 1, 8, 1, 9, 0, 10,
			1, 11, 1, 12, 1, 13, 1, 14, 2, 15, 2, 16, 3, 17, 4, 18, 6, 19, 10,
			20 };


	private static final int[] INFRASTRUCTURE_REQUIRED_ROLE_COORDS_UP = new int[] {
			0, 0, 0, 10, 20, 10, 20, 0 };

	private static final int[] INFRASTRUCTURE_REQUIRED_ROLE_COORDS_LEFT = new int[] {
			0, 0, 10, 0, 10, 20, 0, 20 };

	private static final int[] INFRASTRUCTURE_REQUIRED_ROLE_COORDS_DOWN = new int[] {
			0, 10, 0, 0, 20, 0, 20, 10 };

	private static final int[] INFRASTRUCTURE_REQUIRED_ROLE_COORDS_RIGHT = new int[] {
			10, 0, 0, 0, 0, 20, 20, 20 };

	public AssemblyContextPattern() {
		super(null);
	}

	public String getCreateName() {
		return "AssemblyContext";
	}

	public boolean isMainBusinessObjectApplicable(Object mainBusinessObject) {
		return mainBusinessObject instanceof AssemblyContext
				|| mainBusinessObject instanceof Role;
	}

	protected boolean isPatternControlled(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

	protected boolean isPatternRoot(PictogramElement pictogramElement) {
		Object domainObject = getBusinessObjectForPictogramElement(pictogramElement);
		return isMainBusinessObjectApplicable(domainObject);
	}

	// ADD

	public boolean canAdd(IAddContext context) {
		// check if user wants to add a EClass
		final Object newObject = context.getNewObject();
		if (newObject instanceof AssemblyContext) {
			// check if user wants to add to a diagram
			if (context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	public PictogramElement add(IAddContext context) {
		final AssemblyContext addedAssemblyContext = (AssemblyContext) context
				.getNewObject();
		final Diagram targetDiagram = (Diagram) context.getTargetContainer();

		// CONTAINER SHAPE WITH ROUNDED RECTANGLE
		final IPeCreateService peCreateService = Graphiti.getPeCreateService();
		final ContainerShape containerShape = peCreateService
				.createContainerShape(targetDiagram, true);

		// check whether the context has a size (e.g. from a create feature)
		// otherwise define a default size for the shape
		final int width = context.getWidth() <= 0 ? 200 : context.getWidth();
		final int height = context.getHeight() <= 0 ? 100 : context.getHeight();

		final IGaService gaService = Graphiti.getGaService();
		Rectangle acRectangle; // need to access it later
		{
			// create invisible outer rectangle expanded by
			// the width needed for the anchor
			final Rectangle invisibleRectangle = gaService
					.createInvisibleRectangle(containerShape);
			gaService.setLocationAndSize(invisibleRectangle, context.getX(),
					context.getY(), width, height);

			// create and set visible rectangle inside invisible rectangle
			acRectangle = gaService.createPlainRectangle(invisibleRectangle);
			acRectangle.setStyle(StyleUtil
					.getStyleForAssemblyContext(getDiagram()));
			gaService.setLocationAndSize(acRectangle, 0, 0, width, height);

			// if addedClass has no resource we add it to the resource of the
			// diagram
			// in a real scenario the business model would have its own resource
			if (addedAssemblyContext.eResource() == null) {
				getDiagram().eResource().getContents()
						.add(addedAssemblyContext);
			}

			// create link and wire it
			link(containerShape, addedAssemblyContext);
		}

		// Component Symbol
		{
			// create shape for symbol
			final Shape shape1 = peCreateService.createShape(containerShape,
					false);

			Rectangle invisibleRectangle = gaService
					.createInvisibleRectangle(shape1);
			gaService.setLocationAndSize(invisibleRectangle, width - 15
					- COMP_SYMBOL_WIDTH, 5, COMP_SYMBOL_WIDTH,
					COMP_SYMBOL_HEIGHT);
			invisibleRectangle.setStyle(StyleUtil
					.getStyleForAssemblyContext(getDiagram()));

			// final Shape shape2 = peCreateService.createShape(containerShape,
			// false);
			// final Shape shape3 = peCreateService.createShape(containerShape,
			// false);

			int symelementwidth = (int) ((2.0 / 3.0) * COMP_SYMBOL_WIDTH);
			int symelementxpos = (int) ((1.0 / 3.0) * COMP_SYMBOL_WIDTH);
			int symelementheight = (int) ((4.0 / 15.0) * COMP_SYMBOL_HEIGHT);
			int symelementypos = (int) ((1.0 / 5.0) * COMP_SYMBOL_HEIGHT);

			// create symbol
			final Rectangle rect1 = gaService
					.createRectangle(invisibleRectangle);
			rect1.setStyle(StyleUtil.getStyleForComponentSymbol(getDiagram()));
			gaService.setLocationAndSize(rect1, symelementxpos, 0,
					symelementwidth, COMP_SYMBOL_HEIGHT);

			final Rectangle rect2 = gaService
					.createRectangle(invisibleRectangle);
			rect2.setStyle(StyleUtil.getStyleForComponentSymbol(getDiagram()));
			gaService.setLocationAndSize(rect2, 0, symelementypos,
					symelementwidth, symelementheight);

			final Rectangle rect3 = gaService
					.createRectangle(invisibleRectangle);
			rect3.setStyle(StyleUtil.getStyleForComponentSymbol(getDiagram()));
			gaService.setLocationAndSize(rect3, 0, 3 * symelementypos,
					symelementwidth, symelementheight);
		}

		// SHAPE WITH TEXT
		{
			// create shape for text
			final Shape shape = peCreateService.createShape(containerShape,
					false);

			// create and set text graphics algorithm
			final Text text = gaService.createPlainText(shape,
					addedAssemblyContext.getEntityName());
			text.setStyle(StyleUtil.getStyleForEClassText(getDiagram()));
			text.setVerticalAlignment(Orientation.ALIGNMENT_LEFT);
			gaService.setLocationAndSize(text, 0, 0, width - 40, 20);

			// create link and wire it
			link(shape, addedAssemblyContext);

			// provide information to support direct-editing directly
			// after object creation (must be activated additionally)
			final IDirectEditingInfo directEditingInfo = getFeatureProvider()
					.getDirectEditingInfo();
			// set container shape for direct editing after object creation
			directEditingInfo.setMainPictogramElement(containerShape);
			// set shape and graphics algorithm where the editor for
			// direct editing shall be opened after object creation
			directEditingInfo.setPictogramElement(shape);
			directEditingInfo.setGraphicsAlgorithm(text);
		}

		// add a chopbox anchor to the shape
		peCreateService.createChopboxAnchor(containerShape);

		// create an additional box relative anchor at middle-right
		// final BoxRelativeAnchor boxAnchor = peCreateService
		// .createBoxRelativeAnchor(containerShape);
		// boxAnchor.setRelativeWidth(1.0);
		// boxAnchor.setRelativeHeight(0.38); // Use golden section

		// anchor references visible rectangle instead of invisible rectangle
		// boxAnchor.setReferencedGraphicsAlgorithm(roundedRectangle);

		// assign a graphics algorithm for the box relative anchor
		// final Ellipse ellipse = gaService.createPlainEllipse(boxAnchor);

		// anchor is located on the right border of the visible rectangle
		// and touches the border of the invisible rectangle
		// final int w = INVISIBLE_RECT_RIGHT;
		// gaService.setLocationAndSize(ellipse, -w, -w, 2 * w, 2 * w);
		// ellipse.setStyle(StyleUtil.getStyleForEClass(getDiagram()));

		int rolecount = 0;
		for (ProvidedRole role : addedAssemblyContext
				.getEncapsulatedComponent__AssemblyContext()
				.getProvidedRoles_InterfaceProvidingEntity()) {
			Shape shape = peCreateService.createShape(getDiagram(), true);

			link(shape, role);

			GraphicsAlgorithm algo = null;
			if (role instanceof OperationProvidedRole)
				algo = gaService.createEllipse(shape);
			else if (role instanceof InfrastructureProvidedRole)
				algo = gaService.createRectangle(shape);

			peCreateService.createChopboxAnchor(shape);
			gaService.setLocationAndSize(algo, containerShape
					.getGraphicsAlgorithm().getX() + 35 * rolecount,
					containerShape.getGraphicsAlgorithm().getY()
							+ containerShape.getGraphicsAlgorithm().getHeight()
							+ 40, 20, 20);
			algo.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));

			// One Anchor for each role
			BoxRelativeAnchor anchor = peCreateService
					.createBoxRelativeAnchor(containerShape);
			Rectangle anchorRect = gaService.createInvisibleRectangle(anchor);
			gaService.setLocationAndSize(anchorRect, 35 * rolecount + 9,
					containerShape.getGraphicsAlgorithm().getHeight(), 0, 0);

			// Connect roles and component
			Connection conn = peCreateService
					.createFreeFormConnection(getDiagram());

			// ConnectionDecorator cd =
			// peCreateService.createConnectionDecorator(
			// conn, false, 1.0, true);
			conn.setStart(anchor);
			conn.setEnd(shape.getAnchors().get(0));

			Polyline polyline = gaService.createPolyline(conn);
			polyline.setLineWidth(2);

			rolecount++;
		}

		rolecount = 0;
		for (RequiredRole role : addedAssemblyContext
				.getEncapsulatedComponent__AssemblyContext()
				.getRequiredRoles_InterfaceRequiringEntity()) {
			Shape shape = peCreateService.createShape(getDiagram(), true);

			link(shape, role);

			Rectangle r = gaService.createInvisibleRectangle(shape);

			GraphicsAlgorithm algo = null;
			if (role instanceof OperationRequiredRole) {
				algo = gaService.createPlainPolyline(r,
						OPERATION_REQUIRED_ROLE_COORDS_UP);
			}

			else if (role instanceof InfrastructureRequiredRole)
				algo = gaService.createPlainPolyline(r,
						INFRASTRUCTURE_REQUIRED_ROLE_COORDS_UP);

			peCreateService.createChopboxAnchor(shape);

			gaService.setLocationAndSize(r, containerShape
					.getGraphicsAlgorithm().getX() + 35 * rolecount,
					containerShape.getGraphicsAlgorithm().getY() - 40, 20, 10);

			algo.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));

			// One Anchor for each role
			BoxRelativeAnchor anchor = peCreateService
					.createBoxRelativeAnchor(containerShape);
			Rectangle anchorRect = gaService.createInvisibleRectangle(anchor);
			gaService.setLocationAndSize(anchorRect, 35 * rolecount + 9, 0, 0,
					0);

			// Connect roles and component
			Connection conn = peCreateService
					.createFreeFormConnection(getDiagram());

			// ConnectionDecorator cd =
			// peCreateService.createConnectionDecorator(
			// conn, false, 1.0, true);
			conn.setStart(anchor);
			conn.setEnd(shape.getAnchors().get(0));

			Polyline polyline = gaService.createPolyline(conn);
			polyline.setLineWidth(2);

			rolecount++;
		}

		// call the layout feature
		layoutPictogramElement(containerShape);

		return containerShape;
	}

	// DIRECT EDITING

	public int getEditingType() {
		// there are several possible editor-types supported:
		// text-field, checkbox, color-chooser, combobox, ...
		return TYPE_TEXT;
	}

	@Override
	public boolean canDirectEdit(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		GraphicsAlgorithm ga = context.getGraphicsAlgorithm();
		// support direct editing, if it is a EClass, and the user clicked
		// directly on the text and not somewhere else in the rectangle
		if (bo instanceof AssemblyContext && ga instanceof Text) {
			// EClass eClass = (EClass) bo;
			// additionally the flag isFrozen must be false
			// return !eClass.isFrozen();
			return true;
		}
		// direct editing not supported in all other cases
		return false;
	}

	public String getInitialValue(IDirectEditingContext context) {
		// return the current name of the EClass
		PictogramElement pe = context.getPictogramElement();
		AssemblyContext eClass = (AssemblyContext) getBusinessObjectForPictogramElement(pe);
		return eClass.getEntityName();
	}

	@Override
	public String checkValueValid(String value, IDirectEditingContext context) {
		if (value.length() < 1)
			return "Please enter any text as class name."; //$NON-NLS-1$
		//		if (value.contains(" ")) //$NON-NLS-1$
		//			return "Spaces are not allowed in class names."; //$NON-NLS-1$
		if (value.contains("\n")) //$NON-NLS-1$
			return "Line breakes are not allowed in class names."; //$NON-NLS-1$

		// null means, that the value is valid
		return null;
	}

	@Override
	public void setValue(String value, IDirectEditingContext context) {
		// set the new name for the EClass
		PictogramElement pe = context.getPictogramElement();
		AssemblyContext eClass = (AssemblyContext) getBusinessObjectForPictogramElement(pe);
		eClass.setEntityName(value);

		// Explicitly update the shape to display the new value in the diagram
		// Note, that this might not be necessary in future versions of Graphiti
		// (currently in discussion)

		// we know, that pe is the Shape of the Text, so its container is the
		// main shape of the EClass
		updatePictogramElement(((Shape) pe).getContainer());
	}

	// LAYOUT
	@Override
	public boolean canLayout(ILayoutContext context) {
		// return true, if pictogram element is linked to an EClass
		PictogramElement pe = context.getPictogramElement();
		if (!(pe instanceof ContainerShape))
			return false;
		EList<EObject> businessObjects = pe.getLink().getBusinessObjects();
		return businessObjects.size() == 1
				&& businessObjects.get(0) instanceof AssemblyContext;
	}

	@Override
	public boolean layout(ILayoutContext context) {
		boolean anythingChanged = false;
		ContainerShape containerShape = (ContainerShape) context
				.getPictogramElement();

		context.getPropertyKeys();
		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();
		// the containerGa is the invisible rectangle
		// containing the visible rectangle as its (first and only) child
		GraphicsAlgorithm rectangle = containerGa
				.getGraphicsAlgorithmChildren().get(0);

		// height of invisible rectangle
		if (containerGa.getHeight() < MIN_HEIGHT) {
			containerGa.setHeight(MIN_HEIGHT);
			anythingChanged = true;
		}

		// height of visible rectangle (same as invisible rectangle)
		if (rectangle.getHeight() != containerGa.getHeight()) {
			rectangle.setHeight(containerGa.getHeight());
			anythingChanged = true;
		}

		// width of invisible rectangle
		if (containerGa.getWidth() < MIN_WIDTH) {
			containerGa.setWidth(MIN_WIDTH);
			anythingChanged = true;
		}

		// width of visible rectangle (smaller than invisible rectangle)
		int rectangleWidth = containerGa.getWidth();
		if (rectangle.getWidth() != rectangleWidth) {
			rectangle.setWidth(rectangleWidth);
			anythingChanged = true;
		}

		// width of text and line (same as visible rectangle)
		for (Shape shape : containerShape.getChildren()) {
			GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();
			IGaService gaService = Graphiti.getGaService();
			IDimension size = gaService.calculateSize(graphicsAlgorithm);

			if (shape instanceof Anchor)

				if (graphicsAlgorithm instanceof Ellipse)
					return true;

			if (graphicsAlgorithm instanceof Polyline)
				return true;

			if (rectangleWidth != size.getWidth()
					&& size.getWidth() != COMP_SYMBOL_WIDTH) {
				if (graphicsAlgorithm instanceof Polyline) {
					Polyline polyline = (Polyline) graphicsAlgorithm;
					Point secondPoint = polyline.getPoints().get(1);
					Point newSecondPoint = gaService.createPoint(
							rectangleWidth, secondPoint.getY());
					polyline.getPoints().set(1, newSecondPoint);
					anythingChanged = true;
				} else {
					gaService.setLocation(graphicsAlgorithm, 0, 0);
					gaService.setWidth(graphicsAlgorithm, rectangleWidth - 40);
					// gaService.setHeight(graphicsAlgorithm,
					// containerGa.getHeight());
					anythingChanged = true;
				}
			} else if (size.getWidth() == COMP_SYMBOL_WIDTH) {
				Rectangle rect = (Rectangle) graphicsAlgorithm;
				gaService.setLocationAndSize(rect, containerGa.getWidth()
						- COMP_SYMBOL_WIDTH - 15, rect.getY(), rect.getWidth(),
						rect.getHeight());
				anythingChanged = true;
			}

		}

		return anythingChanged;
	}

	// CREATE

	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	public Object[] create(ICreateContext context) {
		// create EClass
		AssemblyContext newAssemblyContext = CompositionFactory.eINSTANCE
				.createAssemblyContext();
		// Add model element to resource.
		// We add the model element to the resource of the diagram for
		// simplicity's sake. Normally, a customer would use its own
		// model persistence layer for storing the business model separately.
		getDiagram().eResource().getContents().add(newAssemblyContext);

		// Use the following instead of the above line to store the model
		// data in a seperate file parallel to the diagram file
		// try {
		// try {
		// TutorialUtil.saveToModelFile(newClass, getDiagram());
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// } catch (CoreException e) {
		// e.printStackTrace();
		// }

		// do the add
		addGraphicalRepresentation(context, newAssemblyContext);

		// activate direct editing after object creation
		getFeatureProvider().getDirectEditingInfo().setActive(true);
		// return newly created business object(s)
		return new Object[] { newAssemblyContext };
	}

	@Override
	public void moveShape(IMoveShapeContext context) {
		super.moveShape(context);
		int dx = context.getDeltaX();
		int dy = context.getDeltaY();

		int x = context.getX();
		int y = context.getY();

		PictogramElement pe = context.getPictogramElement();

		if (getBusinessObjectForPictogramElement(pe) instanceof AssemblyContext) {
			for (PictogramElement e : getAssociatedRolePes(context
					.getPictogramElement())) {// piList.get(context.getPictogramElement()))
												// {
				e.getGraphicsAlgorithm().setX(
						e.getGraphicsAlgorithm().getX() + dx);
				e.getGraphicsAlgorithm().setY(
						e.getGraphicsAlgorithm().getY() + dy);
			}
			
			
		} else if (getBusinessObjectForPictogramElement(pe) instanceof Role) {
			GraphicsAlgorithm acGa = getAnchorForRolePe(pe).getParent()
					.getGraphicsAlgorithm();
			GraphicsAlgorithm anGa = getAnchorForRolePe(pe)
					.getGraphicsAlgorithm();

			if (x < acGa.getX() && y < acGa.getY() + acGa.getHeight() - 10
					&& acGa.getY() - 10 < y) {
				anGa.setX(0);
				anGa.setY(y - acGa.getY() + 9);
				rotateRoleShape(pe, RoleDirection.LEFT);
			} else if (y < acGa.getY()
					&& x < acGa.getX() + acGa.getWidth() - 10
					&& acGa.getX() - 10 < x) {
				anGa.setX(x - acGa.getX() + 9);
				anGa.setY(0);
				rotateRoleShape(pe, RoleDirection.UP);
			} else if (x > acGa.getX() + acGa.getWidth()
					&& y < acGa.getY() + acGa.getHeight() - 10
					&& acGa.getY() - 10 < y) {
				anGa.setX(acGa.getWidth());
				anGa.setY(y - acGa.getY() + 9);
				rotateRoleShape(pe, RoleDirection.RIGHT);
			} else if (y > acGa.getY() + acGa.getHeight()
					&& x < acGa.getX() + acGa.getWidth() - 10
					&& acGa.getX() - 10 < x) {
				anGa.setX(x - acGa.getX() + 9);
				anGa.setY(acGa.getHeight());
				rotateRoleShape(pe, RoleDirection.DOWN);
			}
		}
	}

	private void rotateRoleShape(PictogramElement pe, RoleDirection direction) {
		if (getBusinessObjectForPictogramElement(pe) instanceof RequiredRole) {
			Rectangle r = (Rectangle) pe.getGraphicsAlgorithm();
			List<GraphicsAlgorithm> c = r.getGraphicsAlgorithmChildren();

			// for (GraphicsAlgorithm g : r.getGraphicsAlgorithmChildren()) {
			// if (g instanceof Polyline) {
			// r.getGraphicsAlgorithmChildren().remove(g);
			// }
			// }

			c.remove(r.getGraphicsAlgorithmChildren().get(0));

			Polyline p;
			if (getBusinessObjectForPictogramElement(pe) instanceof InfrastructureRequiredRole) {
				switch (direction) {
				case LEFT:
					r.setHeight(20);
					r.setWidth(10);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							INFRASTRUCTURE_REQUIRED_ROLE_COORDS_LEFT);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case RIGHT:
					r.setHeight(20);
					r.setWidth(10);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							INFRASTRUCTURE_REQUIRED_ROLE_COORDS_RIGHT);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case UP:
					r.setHeight(10);
					r.setWidth(20);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							INFRASTRUCTURE_REQUIRED_ROLE_COORDS_UP);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case DOWN:
					r.setHeight(10);
					r.setWidth(20);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							INFRASTRUCTURE_REQUIRED_ROLE_COORDS_DOWN);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				default:
					break;

				}
			} else if (getBusinessObjectForPictogramElement(pe) instanceof OperationRequiredRole) {
				switch (direction) {
				case LEFT:
					r.setHeight(20);
					r.setWidth(10);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							OPERATION_REQUIRED_ROLE_COORDS_LEFT);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case RIGHT:
					r.setHeight(20);
					r.setWidth(10);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							OPERATION_REQUIRED_ROLE_COORDS_RIGHT);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case UP:
					r.setHeight(10);
					r.setWidth(20);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							OPERATION_REQUIRED_ROLE_COORDS_UP);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				case DOWN:
					r.setHeight(10);
					r.setWidth(20);
					p = Graphiti.getGaCreateService().createPlainPolyline(r,
							OPERATION_REQUIRED_ROLE_COORDS_DOWN);
					p.setStyle(StyleUtil.getStyleForProvidedRole(getDiagram()));
					break;
				default:
					break;

				}
			}
		}

	}

	@Override
	public boolean canResizeShape(IResizeShapeContext context) {
		if (getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof AssemblyContext) {
			return true;
		}

		return false;
	}

	@Override
	public void resizeShape(IResizeShapeContext context) {
		super.resizeShape(context);
		int direction = context.getDirection();
		ContainerShape containerShape = (ContainerShape) context
				.getPictogramElement();

		int conX = containerShape.getGraphicsAlgorithm().getX();
		int conY = containerShape.getGraphicsAlgorithm().getY();
		int width = containerShape.getGraphicsAlgorithm().getWidth();
		int height = containerShape.getGraphicsAlgorithm().getHeight();

		for (Anchor an : containerShape.getAnchors()) {
			if (an instanceof BoxRelativeAnchor) {
				int x = an.getGraphicsAlgorithm().getX();
				int y = an.getGraphicsAlgorithm().getY();

				switch (direction) {
				case IResizeShapeContext.DIRECTION_WEST:
					if (x + conX + 10 > getRoleGa(an).getX()) {
						an.getGraphicsAlgorithm().setX(0);
					} else {
						an.getGraphicsAlgorithm().setX(width);
					}
					if (getRoleGa(an).getY() != y
							&& getRoleGa(an).getX() > conX
							&& getRoleGa(an).getX() < conX + width) {
						getRoleGa(an).setX(conX + x - 9);
					}
					break;

				case IResizeShapeContext.DIRECTION_EAST:
					if (x + conX + 10 < getRoleGa(an).getX()) {
						an.getGraphicsAlgorithm().setX(width);
					}
					if (getRoleGa(an).getY() != y
							&& getRoleGa(an).getX() > conX
							&& getRoleGa(an).getX() < conX + width) {
						getRoleGa(an).setX(conX + x - 9);
					}
					break;

				case IResizeShapeContext.DIRECTION_NORTH:
					if (y + conY + 10 > getRoleGa(an).getY()) {
						an.getGraphicsAlgorithm().setY(0);
					} else {
						an.getGraphicsAlgorithm().setY(height);
					}
					if (getRoleGa(an).getX() != x
							&& getRoleGa(an).getY() > conY
							&& getRoleGa(an).getY() < conY + height) {
						getRoleGa(an).setY(conY + y - 9);
					}
					break;

				case IResizeShapeContext.DIRECTION_SOUTH:
					if (y + conY + 10 < getRoleGa(an).getY()) {
						an.getGraphicsAlgorithm().setY(height);
					}
					if (getRoleGa(an).getX() != x
							&& getRoleGa(an).getY() > conY
							&& getRoleGa(an).getY() < conY + height) {
						getRoleGa(an).setY(conY + y - 9);
					}
					break;

				default:
					break;

				}
			}
		}
	}

	private GraphicsAlgorithm getRoleGa(Anchor anchor) {
		return anchor.getOutgoingConnections().get(0).getEnd().getParent()
				.getGraphicsAlgorithm();
	}

	private List<PictogramElement> getAssociatedRolePes(PictogramElement pe) {
		AnchorContainer ac = (AnchorContainer) pe;
		List<PictogramElement> roles = new LinkedList<PictogramElement>();

		for (Anchor an : ac.getAnchors()) {
			if (an instanceof BoxRelativeAnchor)
				roles.add(an.getOutgoingConnections().get(0).getEnd()
						.getParent());
		}

		return roles;
	}

	private Anchor getAnchorForRolePe(PictogramElement pe) {
		AnchorContainer ac = (AnchorContainer) pe;
		return ac.getAnchors().get(0).getIncomingConnections().get(0)
				.getStart();
	}

	@Override
	public boolean canRemove(IRemoveContext context) {
		return getBusinessObjectForPictogramElement(context
				.getPictogramElement()) instanceof AssemblyContext;
	}

	@Override
	public void remove(IRemoveContext context) {

		IPeService peService = Graphiti.getPeService();
		for (PictogramElement e : getAssociatedRolePes(context
				.getPictogramElement())) { // piList.get(context.getPictogramElement()))
											// {
			peService.deletePictogramElement(e);
		}
		// piList.remove(context.getPictogramElement());
		super.remove(context);
	}
}