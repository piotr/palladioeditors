package de.upb.swt.palladio.editor.patterns;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.BoxRelativeAnchor;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.pattern.AbstractPattern;
import org.eclipse.graphiti.services.Graphiti;

import de.uka.ipd.sdq.pcm.core.composition.AssemblyConnector;
import de.uka.ipd.sdq.pcm.core.composition.AssemblyContext;
import de.uka.ipd.sdq.pcm.core.composition.AssemblyInfrastructureConnector;
import de.uka.ipd.sdq.pcm.core.composition.Connector;
import de.uka.ipd.sdq.pcm.repository.Role;
import de.uka.ipd.sdq.pcm.system.System;
import de.upb.swt.palladio.editor.StyleUtil;

public class SystemPattern extends AbstractPattern {

	@Override
	public boolean isMainBusinessObjectApplicable(Object mainBusinessObject) {
		return mainBusinessObject instanceof System;
	}

	@Override
	protected boolean isPatternControlled(PictogramElement pictogramElement) {
		return isMainBusinessObjectApplicable(getBusinessObjectForPictogramElement(pictogramElement));
	}

	@Override
	protected boolean isPatternRoot(PictogramElement pictogramElement) {
		return isMainBusinessObjectApplicable(getBusinessObjectForPictogramElement(pictogramElement));
	}

	public boolean canAdd(IAddContext context) {
		// check if user wants to add a EClass
		final Object newObject = context.getNewObject();
		if (newObject instanceof System) {
			// check if user wants to add to a diagram
			if (context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean canCreate(ICreateContext context) {
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		System addedSystem = (System) context.getNewObject();

		ContainerShape shape = Graphiti.getPeCreateService().createContainerShape(getDiagram(), true);
		
		Rectangle containerGA = Graphiti.getGaCreateService().createRectangle(shape);
		containerGA.setStyle(StyleUtil.getStyleForAssemblyContext(getDiagram()));
		Graphiti.getGaService().setLocationAndSize(containerGA, 0, 0, 1500, 2000);
		Polyline line = Graphiti.getGaCreateService().createPlainPolyline(containerGA, new int[] {0,40, containerGA.getWidth(), 40});
		link(shape, addedSystem);
		
		
		int x = 20;
		int y = 20;

		Map<Role, Anchor> roleMap = new HashMap<Role, Anchor>();

		for (AssemblyContext ac : addedSystem
				.getAssemblyContexts__ComposedStructure()) {
			// Create the context information
			AddContext addContext = new AddContext();
			addContext.setNewObject(ac);
			addContext.setTargetContainer(getDiagram());
			addContext.setX(x);
			addContext.setY(y);
			x = x + 250;
			if (x >= 1000) {
				y = y + 250;
				x = 20;
			}

			IAddFeature addFeature = getFeatureProvider().getAddFeature(
					addContext);
			if (addFeature.canAdd(addContext)) {
				PictogramElement pe = addFeature.add(addContext);
				for (Anchor an : ((Shape) pe).getAnchors()) {
					if (an instanceof BoxRelativeAnchor) {
						BoxRelativeAnchor bra = (BoxRelativeAnchor) an;
						Shape role = (Shape) bra.getOutgoingConnections()
								.get(0).getEnd().getParent();
						roleMap.put((Role) role.getLink().getBusinessObjects()
								.get(0), role.getAnchors().get(0));
					}
				}

			}
		}

		for (Connector ac : addedSystem.getConnectors__ComposedStructure()) {

			AddConnectionContext addContext = null;

			if (ac instanceof AssemblyConnector) {
				// AssemblyConnector c = (AssemblyConnector) ac;
				addContext = new AddConnectionContext(
						roleMap.get(((AssemblyConnector) ac)
								.getRequiredRole_AssemblyConnector()),
						roleMap.get(((AssemblyConnector) ac)
								.getProvidedRole_AssemblyConnector()));
				addContext.setNewObject(ac);
				addContext.setTargetContainer((ContainerShape) shape);

			}

			else if (ac instanceof AssemblyInfrastructureConnector) {
				addContext = new AddConnectionContext(
						roleMap.get(((AssemblyInfrastructureConnector) ac)
								.getRequiredRole__AssemblyInfrastructureConnector()),
						roleMap.get(((AssemblyInfrastructureConnector) ac)
								.getProvidedRole__AssemblyInfrastructureConnector()));
				addContext.setNewObject(ac);
				addContext.setTargetContainer((ContainerShape) shape);
			}

			if (addContext != null) {
				IAddFeature addFeature = getFeatureProvider().getAddFeature(
						addContext);
				if (addFeature.canAdd(addContext)) {
					addFeature.add(addContext);
				}
			}

		}

		return null;
	}

}
