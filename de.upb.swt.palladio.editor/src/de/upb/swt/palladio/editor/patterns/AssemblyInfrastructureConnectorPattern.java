package de.upb.swt.palladio.editor.patterns;

import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;

import de.uka.ipd.sdq.pcm.core.composition.AssemblyInfrastructureConnector;
import de.uka.ipd.sdq.pcm.core.composition.CompositionFactory;
import de.uka.ipd.sdq.pcm.repository.InfrastructureProvidedRole;
import de.uka.ipd.sdq.pcm.repository.InfrastructureRequiredRole;
import de.uka.ipd.sdq.pcm.repository.ProvidedRole;
import de.uka.ipd.sdq.pcm.repository.RequiredRole;

public class AssemblyInfrastructureConnectorPattern extends ConnectorPattern
		 {

	public String getCreateName() {
		return "Assembly Infrastructure Connector";
	}

	public AssemblyInfrastructureConnectorPattern() {
		super();
	}

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		// return true if both anchors belong to a EClass
		// and those EClasses are not identical
		RequiredRole source = getInfrastructureRequiredRole(context.getSourceAnchor());
		ProvidedRole target = getInfrastructureProvidedRole(context.getTargetAnchor());
		if (source != null && target != null && source != target) {
			return true;
		}
		return false;
	}

	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		// return true if start anchor belongs to a EClass
		if (getInfrastructureRequiredRole(context.getSourceAnchor()) != null) {
			return true;
		}
		return false;
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		Connection newConnection = null;

		// get EClasses which should be connected
		RequiredRole source = getInfrastructureRequiredRole(context.getSourceAnchor());
		ProvidedRole target = getInfrastructureProvidedRole(context.getTargetAnchor());

		if (source != null && target != null) {
			// create new business object
			AssemblyInfrastructureConnector connector = CompositionFactory.eINSTANCE
					.createAssemblyInfrastructureConnector();
			connector
					.setRequiredRole__AssemblyInfrastructureConnector((InfrastructureRequiredRole) source);
			connector
					.setProvidedRole__AssemblyInfrastructureConnector((InfrastructureProvidedRole) target);

			// add connection for business object
			AddConnectionContext addContext = new AddConnectionContext(
					context.getSourceAnchor(), context.getTargetAnchor());
			addContext.setNewObject(connector);
			newConnection = (Connection) getFeatureProvider().addIfPossible(
					addContext);
		}

		return newConnection;
	}

	/**
	 * Returns the EClass belonging to the anchor, or null if not available.
	 */
	// private EClass getEClass(Anchor anchor) {
	// if (anchor != null) {
	// Object obj = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (obj instanceof EClass) {
	// return (EClass) obj;
	// }
	// }
	// return null;
	// }

	private InfrastructureProvidedRole getInfrastructureProvidedRole(Anchor anchor) {
		if (anchor != null) {
			Object obj = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (obj instanceof InfrastructureProvidedRole) {
				return (InfrastructureProvidedRole) obj;
			}
		}
		return null;
	}

	private InfrastructureRequiredRole getInfrastructureRequiredRole(Anchor anchor) {
		if (anchor != null) {
			Object obj = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (obj instanceof InfrastructureRequiredRole) {
				return (InfrastructureRequiredRole) obj;
			}
		}
		return null;
	}

	@Override
	public boolean canAdd(IAddContext context) {
		// return true if given business object is an EReference
		// note, that the context must be an instance of IAddConnectionContext
		if (context instanceof IAddConnectionContext
				&& context.getNewObject() instanceof AssemblyInfrastructureConnector) {
			return true;
		}
		return false;
	}

}
