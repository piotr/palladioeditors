package de.upb.swt.palladio.editor.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.pattern.DefaultFeatureProviderWithPatterns;

import de.upb.swt.palladio.editor.patterns.AssemblyConnectorPattern;
import de.upb.swt.palladio.editor.patterns.AssemblyContextPattern;
import de.upb.swt.palladio.editor.patterns.AssemblyInfrastructureConnectorPattern;
import de.upb.swt.palladio.editor.patterns.SystemPattern;

public class SystemFeatureProvider extends DefaultFeatureProviderWithPatterns {

   public SystemFeatureProvider(IDiagramTypeProvider dtp) {
      super(dtp);
      addPattern(new SystemPattern());
      addPattern(new AssemblyContextPattern());
      addConnectionPattern(new AssemblyConnectorPattern());
      addConnectionPattern(new AssemblyInfrastructureConnectorPattern());
   }
} 