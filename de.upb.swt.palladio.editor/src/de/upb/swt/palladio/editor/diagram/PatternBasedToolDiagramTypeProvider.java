package de.upb.swt.palladio.editor.diagram;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;

public class PatternBasedToolDiagramTypeProvider extends AbstractDiagramTypeProvider {

   public PatternBasedToolDiagramTypeProvider() {
      super();
      setFeatureProvider(new SystemFeatureProvider(this));
   }
}