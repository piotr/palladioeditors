/*******************************************************************************
 * <copyright>
 *
 * Copyright (c) 2005, 2010 SAP AG.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API, implementation and documentation
 *
 * </copyright>
 *
 *******************************************************************************/
package de.upb.swt.palladio.editor;

import org.eclipse.graphiti.util.IPredefinedRenderingStyle;

public interface IRenderingStyle extends IPredefinedRenderingStyle {
	
	public static final String LIME_WHITE_ID = "lime-white"; //$NON-NLS-1$
	public static final String GREY_WHITE_ID = "grey-white";
}
